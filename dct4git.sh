#!/bin/bash -e
# dct4git - Deep clean tool for git repository
# Copyright (C) 2017 ERCOM - Emeric Verschuur <emeric@mbedsys.org>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

print_critical() {
    printf "\e[91m[ERROR] %s\e[0m\n" "$@"
    exit 1
}

print_note() {
    printf "[NOTE] %s\n" "$@"
}

print_info() {
    printf "\e[92m[INFO] %s\e[0m\n" "$@"
}

# Load bashopts
. $(dirname $0)/3rdparty/bashopts.sh
# Enable backtrace dusplay on error
trap 'bashopts_exit_handle' ERR

bashopts_setup -d "Deep clean tool for git repository" -u "$0 [options] [git repository path]" -s $HOME/.config/dct4gitrc

bashopts_declare -n DCT4GIT_PP_BRANCH -o b -l protect-branches-pattern -d "Protected branches regex pattern" -t string -s -i
bashopts_declare -n DCT4GIT_PP_TAG -o t -l protect-tags-pattern -d "Protected tags regex pattern" -t string -s -i
bashopts_declare -n DCT4GIT_OUT_RM_LIST -o o -l out-rm-list -d "Write remove file list into a file" -t string
bashopts_declare -n DCT4GIT_SELECTOR -o s -l selector \
    -d 'Selector (gawk condition format. The fields ($1, $2 and $3) contain respectively the blob hash, the file size and the file path)' \
    -t string -s -i
bashopts_declare -n DCT4GIT_KEEP_INTERMEDIATE_FILES -o k -l keep-intermediate -d "Keep intermediate files (global_list and protected_blobs_list)"
bashopts_declare -n DCT4GIT_DRY_RUN -o d -l dry-run -d "Dry run mode"
bashopts_declare -n BFG_VERSION -l bfg-version -v "1.12.15" -d "BFG version" -t string -s
bashopts_declare -n BFG_URL -l bfg-url -e "\"http://repo1.maven.org/maven2/com/madgag/bfg/\$BFG_VERSION/bfg-\$BFG_VERSION.jar\"" -d "BFG URL" -t string -s
bashopts_declare -n DCT4GIT_GIT_GC -o c -l git-gc -d "perform a git gc after the clean" -s -v true

# Parse arguments
bashopts_parse_args "$@"

# Process options
bashopts_process_opts

# Extract repository path (default to current place)
if [ -n "${bashopts_commands[0]}" ]; then
    git_repo_dir=$(readlink -f ${bashopts_commands[0]})
else
    git_repo_dir=$PWD
fi

cd $git_repo_dir

# The current directory must be a GIT bare repository (e.g.: using git clone --mirror <url>...)
if [ ! -d "objects/pack" ]; then
    print_critical "$git_repo_dir is not a valid bare git repository (e.g. using git clone --mirror <url>...)"
fi

if [ "$DCT4GIT_KEEP_INTERMEDIATE_FILES" != "true" ]; then
    rm -rf .dct4gitrc
fi
mkdir -p .dct4gitrc

# build the gobal object list with each line under the following format:
#   <blob>|<size>|<file path>
if [ ! -f .dct4gitrc/global_list ]; then
    print_info "Computing global list..." 
    (
        git rev-list --objects --all
        echo # Empty line as separator (endOfBlob2path => 1)
        git verify-pack -v objects/pack/pack-*.idx
    ) | gawk '
        BEGIN {
            # enable first step (blob2path
            #   table build from git rev-list output)
            endOfBlob2path = 0;
        }
        (endOfBlob2path == 0) {
            # blob2path table build
            if (length($0) == 0) {
                # End of first step
                endOfBlob2path = 1;
                next;
            }
            blob2path[$1] = substr($0, 42);
            next;
        }
        /\w{40}\s+blob\s+[0-9]+\s+[0-9]+\s+[0-9]+$/ {
            # Print <blob>|<size>|<file path>
            printf("%s|%s|%s\n", $1, $3, blob2path[$1]);
            blob2size[$1] = $3;
            next;
        }
        /\w{40}\s+blob\s+[0-9]+\s+[0-9]+\s+[0-9]+\s+[0-9]+\s+\w{40}/ {
            # Compute the total file size from all parts
            blobsize = $3 + blob2size[$7];
            # Print <blob>|<size>|<file path>
            printf("%s|%s|%s\n", $1, blobsize, blob2path[$1]);
            blob2size[$1] = blobsize;
            next;
        }
        ' > .dct4gitrc/global_list
fi

# Collecting blob list from protected branches/tags
if [ ! -f .dct4gitrc/protected_blobs_list ]; then
    print_info "Computing protected blob list..."
    if [ -n "$DCT4GIT_PP_BRANCH" ]; then
        for br in $(git branch | sed -E 's/.*\s+//g' | grep -E "$DCT4GIT_PP_BRANCH"); do
            print_note "Processing branch $br..."
            git ls-tree --full-tree -r $br | gawk '/^[0-9]+ blob / { print $3 }' >> .dct4gitrc/protected_blobs_list_unsorted
        done
    fi
    if [ -n "$DCT4GIT_PP_TAG" ]; then
        for br in $(git tag | grep -E "$DCT4GIT_PP_TAG"); do
            print_note "Processing tag $br..."
            git ls-tree --full-tree -r $br | gawk '/^[0-9]+ blob / { print $3 }' >> .dct4gitrc/protected_blobs_list_unsorted
        done
    fi
    print_note "Sorting protected blob list..."
    sort -u .dct4gitrc/protected_blobs_list_unsorted > .dct4gitrc/protected_blobs_list
fi

print_info "Computing blob to delete list..."
(
    cat .dct4gitrc/protected_blobs_list 
    echo # Empry line as separator (endOfProtectedBlobList => 1)
    cat .dct4gitrc/global_list
) | gawk -F '|' '
        BEGIN {
            # enable first step (protected
            #   table build from protected_blobs_list file)
            endOfProtectedBlobList = 0;
        }
        (endOfProtectedBlobList == 0) {
            if (length($0) == 0) {
                # end of protected blob list
                endOfProtectedBlobList = 1;
                next;
            }
            protected[$1]=1;
            next;
        }
        protected[$1] != 1 {
            # Output only not protected blob list 
            print $0;
        }' \
    > .dct4gitrc/delete_blobs_list_begin

print_info "Using the following gawk condition to select the files to delete: '$DCT4GIT_SELECTOR'..."
gawk -F '|' "$DCT4GIT_SELECTOR"' {print $0}' .dct4gitrc/delete_blobs_list_begin > .dct4gitrc/delete_blobs_list_final

if [ -n "$DCT4GIT_OUT_RM_LIST" ]; then
    print_info "Save file list to remove into $DCT4GIT_OUT_RM_LIST"
    sort -u .dct4gitrc/delete_blobs_list_final > $DCT4GIT_OUT_RM_LIST
fi

# Build a list with blobs only (for bfg tool)
gawk -F '|' '{print $1}' .dct4gitrc/delete_blobs_list_final | sort -u > .dct4gitrc/delete_blobs_list

if [ ! -f ${DCT4GIT_TMP_DEP_DIR}/bfg-${BFG_VERSION}.jar ]; then
    curl -s ${BFG_URL} > ${DCT4GIT_TMP_DEP_DIR}/bfg-${BFG_VERSION}.jar \
        || print_critical "Unable to download ${BFG_URL}"
fi

if [ "$DCT4GIT_DRY_RUN" == "true" ]; then
    print_info "Dry run mode: exit now"
    exit 0
fi

print_info "Running bfg cleaner..."
if which java > /dev/null 2>&1; then
    java -jar ${DCT4GIT_TMP_DEP_DIR}/bfg-${BFG_VERSION}.jar --no-blob-protection -bi .dct4gitrc/delete_blobs_list
elif which docker > /dev/null 2>&1; then
    docker run --rm -ti -v ${DCT4GIT_TMP_DEP_DIR}/bfg-${BFG_VERSION}.jar:${DCT4GIT_TMP_DEP_DIR}/bfg-${BFG_VERSION}.jar:ro -v $PWD:$PWD -w $PWD library/openjdk:9-jre \
        java -jar ${DCT4GIT_TMP_DEP_DIR}/bfg-${BFG_VERSION}.jar --no-blob-protection -bi .dct4gitrc/delete_blobs_list
else
    print_critical "bfg need a java runtime"
fi

if [ "$DCT4GIT_GIT_GC" == "true" ]; then
    print_info "Running git gc..."
    git reflog expire --expire=now --all && git gc --prune=now --aggressive
fi

if [ "$DCT4GIT_KEEP_INTERMEDIATE_FILES" != "true" ]; then
    rm -rf .dct4gitrc
fi
